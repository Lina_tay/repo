import logging  
import os  
import sys
import pandas as pd
import openpyxl
from ctk_jinja.jinja import Alert
import importlib.resources as pkg_resources
from sql import dml
from sqlalchemy.sql import text as sa_text
from metadata import metadata_handler as meta
from office365.runtime.auth.client_credential import ClientCredential
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.files.file_creation_information import FileCreationInformation

def download_sharepoint_file(site_url, file_url, download_path, client_id, client_secret, file_name=None):
    ctx = ClientContext.connect_with_credentials(site_url, ClientCredential(client_id, client_secret))

    if file_name:
        download_path = download_path + file_name
    else:
        download_path = os.path.join(download_path, os.path.basename(file_url))

    with open(download_path, "wb") as local_file:
        source_file = ctx.web.get_file_by_server_relative_url(file_url)
        source_file.download(local_file)
        ctx.execute_query()

def upload_sharepoint_file(file_path, site_url, path_url, destination_file_name, client_id, client_secret):
    ctx = ClientContext.connect_with_credentials(site_url, ClientCredential(client_id, client_secret))

    with open(file_path, 'rb') as content_file:
        file_content = content_file.read()

    # upload it into Documents library
    target_folder = ctx.web.get_folder_by_server_relative_url(path_url)

    info = FileCreationInformation()
    info.content = file_content
    info.url = os.path.basename(destination_file_name)
    info.overwrite = True

    target_folder.files.add(info)
    ctx.execute_query()

def send_email(header,run_date, duration,recipients,_args={}):
    alert = Alert(f'Snowflake BUDGET- {header} run Successfully for day {run_date}')
    if len(_args):
        message = f"Total duration for this run is {duration} seconds for the following arguments:"
        alert.add_paragraph(message,'Hello Budget Champions,')

        message =  '<br>'.join(map(str, _args.items()))
        alert.add_paragraph(message)
    else:
        message = f"Total duration for this run is {duration} seconds"
        alert.add_paragraph(message,'Hello Budget Champions,')
        
    paragraph = '''
    Regards,<br>
    Fantastic Team
    '''
    alert.add_paragraph(paragraph)
    alert.render_template()
    alert.send_email(recipients)

def send_system_error_alert(header,recipients,message='',df=pd.DataFrame()):
    alert = Alert('Snowflake- Budget Tool System Error')
    cid = alert.add_header(header)
    if message:
        alert.add_paragraph(message,'Hello Team,')
    if len(df):
        alert.add_attachment(df)
        alert.add_table(df)
    paragraph = '''
    Regards,<br>
    Fantastic Team
    '''
    alert.add_paragraph(paragraph)
    alert.render_template()
    alert.send_email(recipients)


def read_excel(file_path, sheet_name):
    """
    Read the give excel file and extract a specific sheet
    :return: Pandas Dataframe
    """
    with pd.ExcelFile(file_path) as xls:
        logging.info(xls.sheet_names)
        data_xls = pd.read_excel(xls, sheet_name, index_col=None)
        return data_xls

def read_file(filepath):
    """

    This method will accept any file path and load the value of the file into a variable for reading.

    :param filepath: The relative system file path from the base directory
    :return: An object containing the file contents
    """
    try:
        with open(filepath, 'r') as SQL_COMMAND:
            return SQL_COMMAND.read()
    except Exception as e:
        raise Exception(f'Unable to read SQL commands at given file Path : {filepath}')


def select_sql_command_from_file(sql_file,sf_account,placeholder=None):
    """
    Execute the specified SQL file

    :param sql_file: The file containing the individual SQL command
                    to be executed
    :param conn: The connection created from
                the open_database_connection method
    :return:
    """
    # Run generic SQL command
    db = meta.open_sf_connection(sf_account)
    conn = db.connect()
    result = pd.DataFrame()
    sql_statement = read_file(sql_file)
    if placeholder:
        sql_statement = sql_statement.format(**placeholder)
    rs = db.query(conn, sql_statement)
    db.close(conn)
    result = rs.results
    result.columns = map(str.lower, result.columns)
    return result

def execute_sf(sf_account,sql,placeholder=None):
    db = meta.open_sf_connection(sf_account)
    conn = db.connect()
    result = pd.DataFrame()
    if placeholder:
        sql = sql.format(**placeholder)
    rs = db.query(conn, sql)
    db.close(conn)
    result = rs.results
    result.columns = map(str.lower, result.columns)
    return result

def insert_dataframe_sf_table(sf_account, data, database_name, schema_name, table_name):
    """
    truncate given table and insert dataframe into the table
    """
    session, engine = meta.create_sf_engine(sf_account, database_name, schema_name)
    with engine.connect() as sf_conn:
        engine.execute(sa_text(f'TRUNCATE {table_name}').execution_options(autocommit=True))
        data.to_sql(name=table_name, con=sf_conn, if_exists='append',
                    method='multi', chunksize=10000, index=False)


def dataframe_difference(df1, df2, which=None):
    """
    Find rows which are different between two DataFrames.
    which default, return difference rows from both DataFrames
    which:params -> both, left_only, right_only, None
    :return: Dataframe
    """
    comparison_df = df1.merge(df2,
                              indicator=True,
                              how='outer')
    if which is None:
        diff_df = comparison_df[comparison_df['_merge'] != 'both']
    else:
        diff_df = comparison_df[comparison_df['_merge'] == which]
    return diff_df

'''
This method updates corresponing tables in Snowflake for both schemas
W and BR
'''
def update_sf_views_log(sf_account, new_logs, tbl_object, sql=''):  
    new_logs.columns = map(str.upper, new_logs.columns)
    database_name = tbl_object.split('.')[0]
    schema_name = tbl_object.split('.')[1]
    table = tbl_object.split('.')[2]
    session,engine = meta.create_sf_engine(sf_account,database_name,schema_name) 
    with engine.connect() as sf_conn: 
        try:
            if schema_name.upper() == 'W':
                engine.execute(sa_text(f'TRUNCATE {table}').execution_options(autocommit=True))
                new_logs.to_sql(name=table,con=sf_conn,if_exists="append", method='multi',chunksize=10000,index=False)
            else:
                temp_table = table+'_TEMP'
                engine.execute(sa_text(f'DROP TABLE IF EXISTS {temp_table}').execution_options(autocommit=True))
                new_logs.to_sql(name=temp_table,con=sf_conn,method='multi',chunksize=10000,index=False)                
                path = pkg_resources.open_text(dml, sql).name
                sql = read_file(path).format(table,temp_table)
                engine.execute(sa_text(sql).execution_options(autocommit=True))
                engine.execute(sa_text(f'DROP TABLE IF EXISTS {temp_table}').execution_options(autocommit=True))
        except Exception as e:
            if str(e).find(f"{table}' does not exist or not authorized"):
                new_logs.to_sql(name=table,con=sf_conn,if_exists="replace",method='multi',chunksize=10000,index=False)
            else:
                raise Exception(e)
        finally:
            sf_conn.close()
            engine.dispose()

def change_warehouse_size(target_account,size):
    sql = f"Alter warehouse my_wh SET WAREHOUSE_SIZE = {size} "
    execute_sf(target_account,sql)
